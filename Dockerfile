FROM node:18-alpine
ARG POCKETBASE_HOST="https://api.sportsquares.app"

WORKDIR /app

COPY package.json package-lock.json ./
RUN npm install

COPY vite.config.js index.html ./
COPY src src

# Building with Vite reads all environment variables from the .env file
RUN echo VITE_POCKETBASE_URL=$POCKETBASE_HOST > .env

RUN npm run build

EXPOSE 8080

CMD ["npm", "run", "preview"]