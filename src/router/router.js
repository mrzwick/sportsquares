import { createRouter, createWebHistory } from "vue-router";
import profilePage from "@/pages/ProfilePage.vue";
import createBracketPage from "@/pages/CreateBracketPage.vue";
import homePage from "@/pages/HomePage.vue";
import bracketsPage from "@/pages/BracketsPage.vue";
import bracketMembersPage from "@/pages/BracketMembersPage.vue";
import loginPage from "@/pages/LoginPage.vue";
import logoutPage from "@/pages/LogoutPage.vue";
import registerPage from "@/pages/RegisterPage.vue";
import bracketPage from "@/pages/BracketPage.vue";
import editProfilePage from "@/pages/EditProfilePage.vue";
import editBracketPage from "@/pages/EditBracketPage.vue";
import oauthRedirectPage from "@/pages/OAuthRedirectPage.vue";
import privacyPage from "@/pages/PrivacyPage.vue";
import termsPage from "@/pages/TermsPage.vue";
import gettingStartedPage from "@/pages/GettingStartedPage.vue";
import store from "@/store/vuex_store";

const routes = [
  { name: "home", path: "/", component: homePage },
  { name: "login", path: "/login", component: loginPage },
  {
    name: "oauth_redirect",
    path: "/oauth_redirect",
    component: oauthRedirectPage,
  },
  { name: "logout", path: "/logout", component: logoutPage },
  { name: "privacy", path: "/privacy", component: privacyPage },
  { name: "terms", path: "/terms", component: termsPage },
  { name: "register", path: "/register", component: registerPage },
  {
    name: "getting_started",
    path: "/getting-started",
    component: gettingStartedPage,
  },
  {
    name: "list_brackets",
    path: "/brackets",
    component: bracketsPage,
    meta: { requires_auth: true },
  },
  {
    name: "view_bracket",
    path: "/brackets/:id",
    component: bracketPage,
  },
  {
    name: "bracket_members",
    path: "/brackets/:id/members",
    component: bracketMembersPage,
    meta: { requires_auth: true },
  },
  {
    name: "edit_bracket",
    path: "/brackets/:id/edit",
    component: editBracketPage,
    meta: { requires_auth: true },
  },
  {
    name: "new_bracket",
    path: "/brackets/new",
    component: createBracketPage,
    meta: { requires_auth: true },
  },
  {
    name: "profile",
    path: "/profile",
    component: editProfilePage,
    meta: { requires_auth: true },
  },
  {
    name: "edit_profile",
    path: "/profile/:username",
    component: profilePage,
    meta: { requires_auth: true },
  },
];

const router = createRouter({
  // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
  history: createWebHistory(),
  routes, // short for `routes: routes`
});

router.beforeEach((to) => {
  // instead of having to check every route record with
  // to.matched.some(record => record.meta.requiresAuth)
  if (to.meta.requires_auth && !store.getters.active_user) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    return {
      name: "login",
      // save the location we were at to come back later
      query: { redirect: to.fullPath },
    };
  }
});

export default router;
