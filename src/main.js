import { createApp } from "vue";
import App from "./App.vue";
import router from "@/router/router";
import store from "@/store/vuex_store";
import PrimeVue from "primevue/config";
import Dialog from "primevue/dialog";
import SplitButton from "primevue/splitbutton";
import DialogService from "primevue/dialogservice";
import ToastService from "primevue/toastservice";
import Toast from "primevue/toast";
import Textarea from "primevue/textarea";
import InputText from "primevue/inputtext";
import InputNumber from "primevue/inputnumber";
import Dropdown from "primevue/dropdown";
import OverlayPanel from "primevue/overlaypanel";
import ConfirmationService from "primevue/confirmationservice";
import Tooltip from "primevue/tooltip";

import "primevue/resources/themes/saga-blue/theme.css";
import "primevue/resources/primevue.min.css";
import "primeicons/primeicons.css";
import "./assets/main.css";

export const app = createApp(App);
app.use(router).use(store);
app.use(PrimeVue);
app.use(ToastService);
app.use(ConfirmationService);
app.directive("tooltip", Tooltip);

// TODO: Refactor these away into app-specific components that implement these
app.component("Dialog", Dialog);
app.component("SplitButton", SplitButton);
app.component("DialogService", DialogService);
app.component("Toast", Toast);
app.component("Textarea", Textarea);
app.component("InputText", InputText);
app.component("Dropdown", Dropdown);
app.component("InputNumber", InputNumber);
app.component("OverlayPanel", OverlayPanel);

app.mount("#app");
