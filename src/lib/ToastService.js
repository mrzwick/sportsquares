import { ToastSeverity } from "primevue/api";
import { app } from "@/main";

const defaultDuration = 10000;

export function toastSuccess(title, body, duration) {
  app.config.globalProperties.$toast.add({
    severity: ToastSeverity.SUCCESS,
    summary: title,
    detail: body,
    life: duration ?? defaultDuration,
  });
}

export function toastInfo(title, body, duration) {
  app.config.globalProperties.$toast.add({
    severity: ToastSeverity.INFO,
    summary: title,
    detail: body,
    life: duration ?? defaultDuration,
  });
}

export function toastWarn(title, body, duration) {
  app.config.globalProperties.$toast.add({
    severity: ToastSeverity.WARN,
    summary: title,
    detail: body,
    life: duration ?? defaultDuration,
  });
}

export function toastError(title, body, duration) {
  app.config.globalProperties.$toast.add({
    severity: ToastSeverity.ERROR,
    summary: title,
    detail: body,
    life: duration ?? defaultDuration,
  });
}
