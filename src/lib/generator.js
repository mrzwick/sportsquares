const generateBracket = (quarters) => {
  if (quarters !== 1 && quarters !== 2 && quarters !== 4) {
    throw "Number of square quarters must be 1, 2 or 4";
  }
  const bracket = {};
  const number_sets = generateBracketNumbers(quarters * 2);

  bracket.quarter1 = {
    home: number_sets.splice(
      Math.floor(Math.random() * number_sets.length),
      1
    )[0],
    away: number_sets.splice(
      Math.floor(Math.random() * number_sets.length),
      1
    )[0],
  };

  if (quarters === 1) {
    // If we are generating 1 quarter worth of bracket, they apply to the whole game
    bracket.quarter2 = bracket.quarter1;
    bracket.quarter3 = bracket.quarter1;
    bracket.quarter4 = bracket.quarter1;
  } else if (quarters === 2) {
    // If we are generating 2 quarters worth of bracket, they apply to each half
    bracket.quarter2 = bracket.quarter1;
    bracket.quarter3 = {
      home: number_sets.splice(
        Math.floor(Math.random() * number_sets.length),
        1
      )[0],
      away: number_sets.splice(
        Math.floor(Math.random() * number_sets.length),
        1
      )[0],
    };
    bracket.quarter4 = bracket.quarter3;
  } else {
    // Otherwise, every quarter in the bracket has its own scores
    bracket.quarter2 = {
      home: number_sets.splice(
        Math.floor(Math.random() * number_sets.length),
        1
      )[0],
      away: number_sets.splice(
        Math.floor(Math.random() * number_sets.length),
        1
      )[0],
    };
    bracket.quarter3 = {
      home: number_sets.splice(
        Math.floor(Math.random() * number_sets.length),
        1
      )[0],
      away: number_sets.splice(
        Math.floor(Math.random() * number_sets.length),
        1
      )[0],
    };
    bracket.quarter4 = {
      home: number_sets.splice(
        Math.floor(Math.random() * number_sets.length),
        1
      )[0],
      away: number_sets.splice(
        Math.floor(Math.random() * number_sets.length),
        1
      )[0],
    };
  }

  return bracket;
};

const generateBracketNumbers = (quarters) => {
  const sets = [];
  for (let i = 0; i < quarters; i++) {
    const arr = [...Array(10).keys()];
    const set = [];
    while (arr.length > 0) {
      const index = Math.floor(Math.random() * arr.length);
      set.push(arr.splice(index, 1)[0]);
    }
    sets.push(set);
  }

  return sets;
};

export default generateBracket;
