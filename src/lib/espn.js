const base_url = "https://site.api.espn.com/apis/site/v2/sports";

export const getScores = async ({ sport, league, game_name }) => {
  const url = `${base_url}/${sport}/${league}/scoreboard`;
  const events = await fetch(url)
    .then((response) => response.json())
    .then((data) => {
      return data.events;
    });
  return events.filter((event) => event.shortName === game_name)[0];
};
