import { createStore } from "vuex";
import actions from "@/store/actions";
import getters from "@/store/getters";
import mutations from "@/store/mutations";
import state from "@/store/state";

// Create a new store instance.
const store = createStore({
  state,
  getters,
  mutations,
  actions,
});

export default store;
