export default {
  active_user(state) {
    return state.user;
  },
  authenticated(state) {
    return state.user !== null && state.user.id !== null;
  },
  get_bracket: (state) => (id) => {
    return state.brackets[id];
  },
  get_bracket_entries: (state) => (bracket_id) => {
    return state.entries[bracket_id] ?? [];
  },
  get_bracket_members: (state) => (bracket_id) => {
    return state.members[bracket_id] ?? [];
  },
  get_user_brackets: (state) => (user_id) => {
    return state.userBrackets[user_id] ?? [];
  },
  selected_squares(state) {
    return state.selectedSquares;
  },
};
