import client from "@/data/pocketbase";
const initialUserState = client.authStore.model;
export default {
  authStore: client.authStore,
  brackets: {},
  entries: {},
  loginError: null,
  members: {},
  selectedSquares: [],
  userRegisterError: null,
  userRegisterSuccess: false,
  user: initialUserState,
  userBrackets: {},
};
