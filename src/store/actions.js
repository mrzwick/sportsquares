import client from "@/data/pocketbase";
import {
  toastError,
  toastInfo,
  toastSuccess,
  toastWarn,
} from "@/lib/ToastService";
import generateBracket from "@/lib/generator";

export default {
  async login({ commit }, { username, password }) {
    await client
      .collection("users")
      .authWithPassword(username, password)
      .then(() => {
        toastSuccess("You have successfully logged in");
        commit("loginSuccess");
      })
      .catch((e) => {
        toastError("Login failed", e.message);
        throw e;
      });
  },
  oauthLogin: async function (
    { commit },
    { providerName, code, codeVerifier, redirectUrl }
  ) {
    return await client
      .collection("users")
      .authWithOAuth2(providerName, code, codeVerifier, redirectUrl, {
        emailVisibility: true,
      })
      .then(() => {
        toastSuccess("You have successfully logged in");
        commit("loginSuccess");
      })
      .catch((e) => {
        toastError("Login failed", e.message);
        throw e;
      });
  },
  async logout({ commit }) {
    await client.authStore.clear();
    toastInfo("You have logged out");
    commit("logoutSuccess");
  },
  async register(
    { commit },
    { username, email, password, password_confirm, name }
  ) {
    const data = {
      username: username,
      email: email,
      emailVisibility: true,
      password: password,
      passwordConfirm: password_confirm,
      name: name,
    };
    await client
      .collection("users")
      .create(data)
      .then(() => {
        client.collection("users").requestVerification(email);
        commit("registeredUser");
      })
      .catch((e) => {
        const firstKey = Object.keys(e.data.data)[0];
        const errorMessage = e.data.data[firstKey].message;
        toastError("There was a problem creating your account", errorMessage);
        commit("userRegisterError", errorMessage);
      });
  },
  clear_selected_squares({ commit }) {
    commit("clearSelectedSquares");
  },
  async createNewBracket(
    { state },
    { name, game_id, description, number_sets }
  ) {
    const data = {
      name: name,
      game_id: game_id,
      owner: state.user.id,
      description: description,
      number_sets: number_sets,
    };
    return await client
      .collection("brackets")
      .create(data)
      .catch((e) => {
        toastError("There was a problem creating your bracket.", e.message);
      });
  },
  async deleteBracket({ commit }, { bracket_id }) {
    return await client
      .collection("brackets")
      .delete(bracket_id)
      .then(() => {
        toastSuccess("Bracket has been deleted.");
      })
      .catch((e) => {
        toastError("There was a problem deleting the bracket.");
        commit("deleteBracketError", e);
      });
  },
  async editBracket(
    {},
    { bracket_id, name, game_id, description, number_sets }
  ) {
    const data = {
      name: name,
      game_id: game_id,
      description: description,
      number_sets: number_sets,
    };
    return await client
      .collection("brackets")
      .update(bracket_id, data)
      .then(() => {
        toastSuccess("Bracket has been updated.");
      })
      .catch((e) => {
        toastError("There was a problem editing the bracket.", e.message);
      });
  },
  async fetchBracket({ state }, { id }) {
    const expand = "owner,game_id";

    return await client
      .collection("brackets")
      .getOne(id, { expand: expand })
      .then((response) => {
        client.collection("brackets").subscribe(id, (response) => {
          state.brackets[id] = response.record;
        });
        state.brackets[id] = response;
      })
      .catch((e) => {
        toastError("There was a problem finding the bracket.", e.message);
      });
  },
  async fetchUserBrackets({ state }, { user_id }) {
    const expand = "bracket,bracket.owner,bracket.game_id";
    const filter = `user="${user_id}"`;
    return await client
      .collection("bracket_users")
      .getList(1, 25, { expand: expand, filter: filter })
      .then((brackets) => {
        state.userBrackets[user_id] = brackets.items;

        // Subscribe to all future changes to the users brackets and update the state with them
        client.collection("bracket_users").subscribe("*", (response) => {
          if (response.record.user === user_id) {
            // Pocketbase sometimes doesn't return records if we fetch immediately. Add a small arbitrary delay
            setTimeout(() => {
              client
                .collection("bracket_users")
                .getList(1, 25, { expand: expand, filter: filter })
                .then((brackets) => {
                  state.userBrackets[user_id] = brackets.items;
                });
            }, 100);
          }
        });
      })
      .catch((e) => {
        toastError("There was a problem finding the brackets.", e.message);
      });
  },
  async editMember({}, { id, entries }) {
    const data = {
      entries: entries,
    };
    return await client.collection("bracket_users").update(id, data);
  },
  async removeMember({}, { id }) {
    return await client.collection("bracket_users").delete(id);
  },
  async fetchMembers({ state }, { id }) {
    const filter = `bracket="${id}"`;
    const expand = "user";
    return await client
      .collection("bracket_users")
      .getList(1, 250, { filter: filter, expand: expand })
      .then((members) => {
        state.members[id] = members.items;

        // Subscribe to all future changes to the users brackets and update the state with them
        client.collection("bracket_users").subscribe("*", (response) => {
          if (response.record.bracket === id) {
            // Pocketbase sometimes doesn't return records if we fetch immediately. Add a small arbitrary delay
            setTimeout(() => {
              client
                .collection("bracket_users")
                .getList(1, 250, { filter: filter, expand: expand })
                .then((members) => {
                  state.members[id] = members.items;
                });
            }, 100);
          }
        });
      });
  },
  async fetchSquares({ state, commit }, { id }) {
    const filter = `bracket_id = "${id}"`;
    const expand = "bracket_id,user_id,owner,membership_id";
    return await client
      .collection("entries")
      .getList(1, 100, { filter: filter, expand: expand })
      .then((response) => {
        state.entries[id] = response.items;

        // Subscribe to all future updates to entries and update the state with them
        client.collection("entries").subscribe("*", (response) => {
          if (response.record.bracket_id === id) {
            // Pocketbase sometimes doesn't return records if we fetch immediately. Add a small arbitrary delay
            setTimeout(() => {
              client
                .collection("entries")
                .getList(1, 100, { filter: filter, expand: expand })
                .then((response) => {
                  state.entries[id] = response.items;
                });
            }, 100);
          }
        });
      })
      .catch((e) => {
        toastWarn(
          "There was a problem getting the squares for this bracket",
          e.message
        );
        commit("fetchSquaresError", e);
      });
  },
  async generateBracketNumbers({}, { bracket_id, quarters }) {
    const bracket = generateBracket(quarters);
    return await client
      .collection("brackets")
      .update(bracket_id, { squares: bracket })
      .catch((e) => {
        toastError("Failed to generate Bracket numbers.", e.message);
      });
  },
  async addManualBracketEntry({}, { bracket_id, entries, name }) {
    const num_entries = entries ?? 0;
    const data = {
      bracket: bracket_id,
      custom_name: name,
      entries: num_entries,
    };
    return await client
      .collection("bracket_users")
      .create(data)
      .catch((e) => {
        toastError(
          "There was a problem adding the user to the bracket",
          e.message
        );
        throw e;
      });
  },
  async joinBracket({ state }, { bracket_id, entries }) {
    const num_entries = entries ?? 0;
    const data = {
      bracket: bracket_id,
      user: state.user.id,
      entries: num_entries,
    };
    return await client
      .collection("bracket_users")
      .create(data)
      .catch((e) => {
        toastError("There was a problem joining the bracket", e.message);
        throw e;
      });
  },
  async editUser({ commit }, { user_id, username, email, name, color }) {
    return await client
      .collection("users")
      .update(user_id, {
        username: username,
        email: email,
        name: name,
        color: color,
      })
      .then(() => {
        toastSuccess("Updated your profile");
      })
      .catch((e) => {
        const firstKey = Object.keys(e.data.data)[0];
        const errorMessage = e.data.data[firstKey].message;
        toastError("There was a problem updating your profile", errorMessage);
        commit("updateProfileError", errorMessage);
      });
  },
  select_square({ commit }, { number }) {
    commit("selectedSquare", number);
  },
  unselect_square({ commit }, { number }) {
    commit("unselectedSquare", number);
  },
  async reserve_selected_squares(
    { commit, state, dispatch },
    { bracket_id, membership_id }
  ) {
    state.selectedSquares.forEach((number, index) => {
      const matching_entries = state.entries[bracket_id].filter((entry) => {
        return entry.entry_number === number;
      });
      if (matching_entries.length) {
        toastError(
          `Could not reserve Square ${number}, it has already been picked`
        );
        return;
      }
      setTimeout(() => {
        dispatch("reserve_square", {
          bracket_id: bracket_id,
          user_id: state.user.id,
          entry_number: number,
          membership_id: membership_id,
        });
      }, 100 * index);
    });

    commit("clearSelectedSquares");
  },
  clear_square({}, { entry_id }) {
    client.collection("entries").delete(entry_id);
  },
  async reserve_square(
    {},
    { bracket_id, user_id, entry_number, membership_id }
  ) {
    const data = {
      user_id: user_id,
      bracket_id: bracket_id,
      entry_number: entry_number,
      membership_id: membership_id,
    };

    await client
      .collection("entries")
      .create(data, { $autoCancel: false })
      .catch(() => {
        toastWarn(`Unable to reserve square ${entry_number}`);
      });
  },
};
