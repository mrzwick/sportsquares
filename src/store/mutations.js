export default {
  loginSuccess(state) {
    state.user = state.authStore.model;
  },
  logoutSuccess(state) {
    state.user = state.authStore.model;
  },
  registeredUser(state) {
    state.user = state.authStore.model;
    state.userRegisterSuccess = true;
  },
  clearSelectedSquares(state) {
    state.selectedSquares = [];
  },
  selectedSquare(state, number) {
    state.selectedSquares.push(number);
  },
  unselectedSquare(state, number) {
    state.selectedSquares = state.selectedSquares.filter((x) => x !== number);
  },
};
