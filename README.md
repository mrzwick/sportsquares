# SportSquares

This is an application for creating Superbowl Square style brackets and allowing users to share and sign up for them.

## Project Setup

```sh
npm i
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

### Run locally using Docker-compose

```sh
docker-compose build
docker-compose up
```

### Bootstrapping Pocketbase **(Initial run)**
The first time you run the application locally using docker-compose, you'll need to setup the Pocketbase instance.

Navigate to the Pocketbase Admin URL displayed by docker-compose output (usually http://0.0.0.0:8090/_/). Follow the steps to setup the Administrative user.

Once you are in the Pocketbase dashboard, go to `Settings` -> `Import Collections` and upload the `pocketbase/pb_schema.json` file.

## Contributing
If you would like to contribute to this project, please open a merge request. In your commit, please provide an explanation of changes and the desired effect of the changes to the application.

If you encounter any problems, please open an issue in the repository describing the problem and expected outcome.
